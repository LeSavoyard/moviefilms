<?php

namespace App\Controller;

use App\Entity\Films;
use App\Entity\User;
use App\Form\EditFilmType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/addFilm", name="add_film")
     * @Route("/film/{id}/edit", name="edit_film")
     */
    public function editFilm(Films $film = null, Request $request, EntityManagerInterface $manager)
    {
        if (!$film) {
            $film = new Films();
        }
        

        $form = $this->createForm(EditFilmType::class,$film);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($film);
            $manager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('admin/edit_film.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/film/{id}/delete", name="delete_film")
     */
    public function deleteFilm(Films $film, EntityManagerInterface $manager)
    {
        $manager->remove($film);
        $manager->flush();
        return $this->redirectToRoute('all_film');
    }
    /**
     * @Route("admin/users", name="gestion_user")
     */
    public function gestionUser(UserRepository $repo)
    {
        return $this->render('admin/gestion_user.html.twig', [
            'users' => $repo->findAll()
        ]);
    }

    /**
     * @Route("admin/user/{id}/change", name="change_role")
     */
    public function FunctionName(User $user, EntityManagerInterface $manager)
    {
        if (in_array("ROLE_ADMIN", $user->getRoles())) {
            $user->setRoles([]);
        }
        else {
            $user->setRoles(["ROLE_ADMIN"]);
        }
        $manager->persist($user);
        $manager->flush();
        return $this->redirectToRoute('gestion_user');
    }
}
