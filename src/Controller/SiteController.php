<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Entity\Films;
use App\Entity\User;
use App\Form\AvisType;
use App\Repository\FilmsRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('site/index.html.twig', [
        ]);
    }
    /**
     * @Route("/film/all", name="all_film")
     */
    public function allFilms(FilmsRepository $repo)
    {
        $films = $repo->findAll();
        return $this->render('site/all_films.html.twig', [
            'films' => $films
        ]);
    }
    /**
     * @Route("/film/{id}", name="film_page")
     */
    public function filmPage(Films $film)
    {
        return $this->render('site/page_film.html.twig', [
            'film' => $film,
            'avis' => $film->getAvis()
        ]);
    }
    /**
     * @Route("/film/{id}/avis/new", name="new_avis")
     */
    public function newAvis(Films $film, EntityManagerInterface $manager, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $avis = new Avis();
        $avis->setAuteur($this->getUser());
        $avis->setFilm($film);
        $avis->setDate(new \DateTime());
        $form = $this->createForm(AvisType::class, $avis);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($avis);
            $manager->flush();

            return $this->redirectToRoute('film_page', ['id' => $film->getId()]);
        }

        return $this->render('site/new_avis.html.twig', [
            'form' =>$form->createView()
        ]);
        
    }
    /**
     * @Route("/user/{id}/dashboard", name="dashboard")
     */
    public function dashboard(User $user)
    {
        return $this->render('site/dashboard.html.twig', [
            'user' => $user
        ]);
    }
}
