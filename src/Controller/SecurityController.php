<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditProfilType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function addProfil(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        $form = $this->createForm(EditProfilType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();
            if ($this->isGranted("ROLE_ADMIN")) {
                return $this->redirectToRoute('gestion_user');
            }
            else{
                return $this->redirectToRoute('index');
            }
        }
        return $this->render('security/editProfil.html.twig', [
            'form'=> $form->createView()
        ]);
    }
    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(){}
    /**
     * @Route("/profil/{id}/edit", name="edit_profil")
     */
    public function editProfil(User $user, EntityManagerInterface $manager, Request $request)
    {
        if ($this->getUser() and !$this->isGranted("ROLE_ADMIN")) {
            $this->redirectToRoute('index');
        }
        $form = $this->createFormBuilder($user)
                        ->add('username', TextType::class)
                        ->add('nom', TextType::class)
                        ->add('prenom', TextType::class)
                        ->add('age', TextType::class)
                        ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();

            if ($this->getUser()->getId() == $user->getId()) {
                return $this->redirectToRoute('index');
            }
            elseif ($this->isGranted("ROLE_ADMIN")) {
                return $this->redirectToRoute('gestion_user');
            }
        }
        return $this->render('security/editProfil.html.twig', [
                'form' => $form->createView()
            ]);
    }
    /**
     * @Route("/profil/{id}/editpassword", name="edit_password")
     * @Route("/profil/{id}/forgotpassword", name ="redo_password")
     */
    public function editPassword(User $user, EntityManagerInterface $manager, Request $request, UserPasswordEncoderInterface $encoder)
    {
        if (($this->getUser() && $this->getUser()->getId() != $user->getId()) && "redo_password" != $request->attributes->get('_route')) {
            return $this->redirectToRoute('index');
        }
        $form = $this->createFormBuilder($user)
                        ->add('password', PasswordType::class)
                        ->add('confirm_password', PasswordType::class)
                        ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();
            if("redo_password" == $request->attributes->get('_route')){
                return $this->redirectToRoute('index');
            }
            else{
                return $this->redirectToRoute('dashboard', ['id'=>$user->getId()]);
            }
        }
        return $this->render('security/editProfil.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/user/{id}/delete", name="user_delete")
     */
    public function deleteUser(User $user, EntityManagerInterface $manager)
    {
        if ($this->getUser() and !$this->isGranted("ROLE_ADMIN")) {
            $this->redirectToRoute('index');
        }
        else{
            if ($user->getId() == $this->getUser()->getId()){
                $this->logout();
            }
            $manager->remove($user);
            $manager->flush();
            if ($this->getUser()->getId() == $user->getId()) {
                return $this->redirectToRoute('index');
            }
            elseif ($this->isGranted("ROLE_ADMIN")) {
                return $this->redirectToRoute('gestion_user');
            }
        }
    }
    /**
     * @Route("/login/forgot", name="forgot")
     */
    public function forgot()
    {
        return $this->render('security/forgot.html.twig');
    }
    /**
     * @Route("/login/forgot/send", name="get_infos")
     */
    public function send(UserRepository $repo, Swift_Mailer $mailer)
    {
        $user = $repo->findOneBy(['email' => $_POST['email']]);

        if ($user) {
            $message = (new \Swift_Message('Hello Email'))
                        ->setFrom('send@example.com')
                        ->setTo($user->getEmail())
                        ->setBody(
                            $this->renderView(
                                'security/mail.html.twig',
                                ['user' => $user]
                            ),
                            'text/html'
                        );

        $mailer->send($message);

        return $this->render('security/confirm.html.twig', [
            'user' => $user
        ]);
        }
        else{
            return $this->redirectToRoute('forgot');
        }
        
    }
}
